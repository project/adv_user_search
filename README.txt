INTRODUCTION
------------
The Advanced user search module provides a user search page that is similar
to the user search provided by Drupal, but that is more flexible, and
by default provides a better search experience for the user.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/adv_user_search

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/adv_user_search


REQUIREMENTS
------------
This module requires the core search module.


INSTALLATION
------------
Install as you would normally install a contributed Drupal module.
See: https://drupal.org/documentation/install/modules-themes/modules-7
for further information.


CONFIGURATION
-------------

 * Go to the search settings page in:

   Administration » Configuration » Search and metadata » Search settings

 * Activate Advanced user search in the "Active search module" section

 * After activating the Advanced user search module, review the
   Advanced user search module section that will appear in the Search
   settings for additional options


MAINTAINERS
-----------
Current maintainers:

 * Flávio Veloso (flaviovs) - https://drupal.org/u/flaviovs
